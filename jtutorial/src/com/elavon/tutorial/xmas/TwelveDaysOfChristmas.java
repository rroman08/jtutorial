/**
 * 
 */
package com.elavon.tutorial.xmas;

/**
 * @author Randy
 *
 */
public class TwelveDaysOfChristmas {

	/**
	 * 
	 */
	public TwelveDaysOfChristmas() {
	}
	
	public static void printLyrics() {
		
		String s1 = "";
		String s2 = "";
		
		for (int i=1; i <=12; i++) {
			switch (i) {
			case 1: {
				s1 = "On the first day of Christmas my true love sent to me \n";
				s2 = "a Partridge in a Pear Tree\n";
				System.out.println(s1 + s2);
				break;
			}
			case 2: {
				s1 = "On the second day of Christmas my true love sent to me \n";
				s2 = "Two Turtle Doves \n"+ "and " + s2;
				System.out.println(s1 + s2);
				break;				
			}
				
			case 3: {
				s1 = "On the third day of Christmas my true love sent to me \n";
				s2 = "Three French Hens \n" + s2;
				System.out.println(s1 + s2);
				break;								
			}
			case 4: {
				s1 = "On the fourth day of Christmas my true love sent to me \n";
				s2 = "Four Calling Birds \n" + s2;
				System.out.println(s1 + s2);
				break;								
			}
			case 5: {				
				s1 = "On the fifth day of Christmas my true love sent to me \n";
				s2 = "Five Golden Rings \n" + s2;
				System.out.println(s1 + s2);
				break;								
			}
			case 6: {
				s1 = "On the sixth day of Christmas my true love sent to me \n";
				s2 = "Six Geese a Laying \n" + s2;
				System.out.println(s1 + s2);
				break;								
			}
			case 7: {				
				s1 = "On the seventh day of Christmas my true love sent to me \n";
				s2 = "Seven Swans a Swimming \n" + s2;
				System.out.println(s1 + s2);
				break;								
			}
			case 8: {
				s1 = "On the eight day of Christmas my true love sent to me \n";
				s2 = "Eight Maids a Milking \n" + s2;
				System.out.println(s1 + s2);
				break;												
			}
			case 9: {
				s1 = "On the ninth day of Christmas my true love sent to me \n";
				s2 = "Nine Ladies Dancing \n" + s2;
				System.out.println(s1 + s2);
				break;																
			}
			case 10: {				
				s1 = "On the tenth day of Christmas my true love sent to me \n";
				s2 = "Ten Lords a Leaping \n" + s2;
				System.out.println(s1 + s2);
				break;																
			}
			case 11: {				
				s1 = "On the eleventh day of Christmas my true love sent to me \n";
				s2 = "Eleven Pipers Piping \n" + s2;
				System.out.println(s1 + s2);
				break;																
			}
			case 12: {
				s1 = "On the twelfth day of Christmas my true love sent to me \n";
				s2 = "Tweleve Drummers Drumming \n" + s2;
				System.out.println(s1 + s2);
				break;																
			}
			}
		}
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}

}
