/**
 * 
 */
package com.elavon.tutorial.tv;

/**
 * SmartTV class. 
 * @author Randy Roman
 *
 */
public class SmartTV extends ColoredTV {

	private NetworkConnection network = null;
	
	/**
	 * Creates an instance of a ColoredTV class.
	 * @param sBrand - Manufacturer's name
	 * @param sModel - TV Model
	 */
	public SmartTV(String sBrand, String sModel) {
		super(sBrand, sModel);
		this.network = new NetworkConnectionImpl();
	}
	
	public void connectToNetwork (String sNetworkName) {
		this.network.connect(sNetworkName);
	}
	
	public boolean hasNetworkConnection( ) {
		return network != new NetworkConnectionImpl();
	}
	
	public String toString( ) {
		strDesc = super.getDescription() + this.network;
		return strDesc;		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SmartTV tv = new SmartTV("SAMSUNG", "SMTV1");
		tv.turnOn();
		for (short i=0; i<3; i++) {
			tv.brightnessUp();
			tv.channelUp();
			tv.pictureUp();
		}
		
		for (short i=5; i>0; i--) {
			tv.brightnessDown();
			tv.channelDown();
			tv.pictureUp();
		}
		
		tv.pictureDown();
		tv.channelUp();
		tv.channelUp();
		
		System.out.println(tv);
		
		tv.connectToNetwork("TestNetwork");

		System.out.println(tv);
	}

}
