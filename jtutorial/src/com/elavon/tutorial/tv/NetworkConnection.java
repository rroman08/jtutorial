/**
 * 
 */
package com.elavon.tutorial.tv;

/**
 * NetworkConnection interface.
 * @author Randy Roman
 *
 */
public interface NetworkConnection {
	
	/**
	 * Attempts to connect and establish a network connection.
	 * 
	 * @param s Network Name
	 * @return an instance of an established NetworkConnection
	 */
	public NetworkConnection connect(String s);
	
	/**
	 * Returns the status if there is an established network connection
	 * @return true if there is established connection; false if no connection.
	 */
	public boolean connectionStatus( );
	
	//public boolean hasNetworkCoonection();
}
