/**
 * 
 */
package com.elavon.tutorial.tv;

/**
 * Exercise 2 - Create a TV class
 * @author Randy Roman
 *
 */
public class TV {
	/**
	 * Maximum number of channels.
	 */
	public static final int nMaxChannel = 13;
	
	/**
	 * Maximum volume.
	 */
	public static final int nMaxVol = 10;

	/**
	 * Default Channel is zero.
	 */
	public static final int nDefaultChannel = 0;
	
	/**
	 * Default Volume is 5.
	 */
	public static final int nDefaultVol = 5;
	
	/**
	 * Manufacturer name. 
	 */
	protected String brand;
	
	/**
	 * Model name
	 */
	protected String model;
	
	/**
	 * Power On/Off
	 */
	protected boolean powerOn = false;
	
	/**
	 * Current channel.
	 */
	protected int channel = 0;

	/**
	 * Current volume level.
	 */
	protected int volume = 0;
	
	/**
	 * Creates an instance of TV class and sets the default channel to 0, 
	 * and volume to 5.
	 * 
	 * @param sBrand - Manufacturer's name
	 * @param sModel - TV Model
	 */
	public TV (String sBrand, String sModel) {
		this.brand = sBrand;
		this.model = sModel;
		this.channel = nDefaultChannel;
		this.volume = nDefaultVol;
	}
	
	/**
	 * Sets the powerOn = true;
	 */
	public void turnOn( ) {
		this.powerOn = true;
	}

	/**
	 * Sets the powerOn = false;
	 */
	public void turnOff( ) {
		this.powerOn = false;
	}
	
	/**
	 * Increments the channel. If current channel is already max, then loop back to 0;
	 * 
	 * @return new channel
	 */
	public int channelUp( ) {
		if (this.powerOn && this.channel == nMaxChannel) {
			this.channel = 0;	// reset to 0
		} else {
			this.channel++;
		}
		
		return this.channel;
	}
	
	/**
	 * Decrements the channel. If current channel is already lowest, then loop up to max channel;
	 * @return new channel
	 */
	public int channelDown( ) {
		if (this.powerOn && this.channel == 0) {
			this.channel = nMaxChannel;	// reset to highest channel
		} else {
			this.channel--;
		}
		
		return this.channel;
	}

	/**
	 * Increments the volume if current volume is not yet max;
	 * @return new volume level
	 */
	public int volumeUp( ) {
		if (this.powerOn && this.volume < nMaxVol) {
			this.volume++;	
		}
		
		return this.volume;
	}

	
	/**
	 * Decrements the volume if current volume is not yet 0;
	 * @return new volume level
	 */
	public int volumeDown( ) {
		if (this.powerOn && this.volume > 0) {
			this.volume--;	
		}
		
		return this.volume;
	}
	
	String strDesc = new String();
	
	String getDescription( ) {
		strDesc = this.brand + " " + this.model + " [on:" + this.powerOn + ", channel:" + this.channel + ", volume:" + this.volume + "]";
		return strDesc;
	}
	
	
	@Override
	public String toString() {
		return getDescription();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TV tv = new TV("Andre Electronics", "ONE");
		System.out.println(tv);
		
		tv.turnOn();
		for (short i=0; i < 5; i++) {
			tv.channelUp();
		}
		
		tv.channelDown();

		for (short i=3; i > 0; i--) {
			tv.volumeDown();
		}
		
		tv.volumeUp();
		tv.turnOff();
		
		System.out.println(tv);
	}

}
