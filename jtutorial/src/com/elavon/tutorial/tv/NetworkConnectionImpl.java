/**
 * 
 */
package com.elavon.tutorial.tv;

/**
 * NetworkConnectionImpl class
 * @author Randy Roman
 *
 */
public class NetworkConnectionImpl implements NetworkConnection {

	//private boolean bStatus;
	
	private String sNetworkName = null;
	
	/**
	 * Instantiates a NetworkConnectionImpl class.
	 */
	public NetworkConnectionImpl() {
	}

	/* (non-Javadoc)
	 * @see com.elavon.tutorial.tv.NetworkConnection#connect()
	 */
	@Override
	public NetworkConnection connect(String sNetworkName) {
		this.sNetworkName = sNetworkName;
		
		return this;
	}

	/* (non-Javadoc)
	 * @see com.elavon.tutorial.tv.NetworkConnection#connectionStatus()
	 */
	@Override
	public boolean connectionStatus() {
		return sNetworkName != null;
	}
	
	public String toString() {
		return "[Connection Status:" + this.connectionStatus() + ", Network Name:" + this.sNetworkName + "]";
	}

}
