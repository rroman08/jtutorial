/**
 * 
 */
package com.elavon.tutorial.tv;

/**
 * ColoredTV class
 * @author Randy Roman
 *
 */
public class ColoredTV extends TV {
	private static final int nMaxBrightness = 100;
	private static final int nMaxContrast = 100;
	private static final int nMaxPix = 100;
	private static final int nDefaultBrightness = 50;
	private static final int nDefaultContrast = 50;
	private static final int nDefaultPix = 50;
	
	protected int brightness = 50;
	protected int contrast = 50;
	protected int picture = 50;
	
	
	/**
	 * Increments the brightness if current level is not yet max;
	 * @return new brightness level
	 */
	public int brightnessUp( ) {
		if (super.powerOn && this.brightness < nMaxBrightness) {
			this.brightness++;	
		}
		
		return this.brightness;
	}

	
	/**
	 * Decrements the brightness if current level is not yet 0;
	 * @return new brightness level
	 */
	public int brightnessDown( ) {
		if (super.powerOn && this.brightness > 0) {
			this.brightness--;	
		}
		
		return this.brightness;
	}
	

	/**
	 * Increments the contrast if current level is not yet max;
	 * @return new contrast level
	 */
	public int contrastUp( ) {
		if (super.powerOn && this.contrast < nMaxContrast) {
			this.contrast++;	
		}
		
		return this.contrast;
	}

	
	/**
	 * Decrements the contrast if current level is not yet 0;
	 * @return new contrast level
	 */
	public int contrastDown( ) {
		if (super.powerOn && this.contrast > 0) {
			this.contrast--;	
		}
		
		return this.contrast;
	}
	
	/**
	 * Increments the picture if current level is not yet max;
	 * @return new picture level
	 */
	public int pictureUp( ) {
		if (super.powerOn && this.picture < nMaxPix) {
			this.picture++;	
		}
		
		return this.picture;
	}

	
	/**
	 * Decrements the picture if current level is not yet 0;
	 * @return new picture level
	 */
	public int pictureDown( ) {
		if (super.powerOn && this.picture > 0) {
			this.picture--;	
		}
		
		return this.picture;
	}

	/**
	 * @throws Exception if nChannel is unsupported
	 * @param nChannel new channel to set
	 * @return new channel
	 */
	public int switchToChannel(int nChannel) throws Exception {
		if (super.powerOn && (nChannel < 0 || nChannel > nMaxChannel))
			throw new Exception("This channel is unsupported. Please choose between 0 to " + nMaxChannel + ".");
		
		this.channel = nChannel;
		return this.channel;
	}
	
	/**
	 * Mutes the volume level to 0.
	 * @return 0 volume level
	 */
	public int mute( ) {
		if (super.powerOn) this.volume = 0;
		return this.volume;
	}
	
	//String strDesc = new String();
	
	String getDescription( ) {
		strDesc = super.getDescription() + "[b:" + this.brightness + ", c:" + this.contrast + ", p:" + this.picture + "]";
		return strDesc;
	}
		
	
	/**
	 * Creates an instance of a ColoredTV class.
	 * @param sBrand - Manufacturer's name
	 * @param sModel - TV Model
	 */
	public ColoredTV(String sBrand, String sModel) {
		super(sBrand, sModel);
		this.brightness = nDefaultBrightness;
		this.contrast = nDefaultContrast;
		this.picture = nDefaultPix;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		TV bnwTV = new TV ("Admiral", "A1");
		TV sonyTV = new ColoredTV("SONY", "S1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		
		if (sonyTV instanceof ColoredTV)
			((ColoredTV) sonyTV).brightnessUp();

	
		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
		
		// need to turn on TV first before calling any adjustments
		sharpTV.turnOn();
		
		sharpTV.mute();
		
		for (short i=0; i < 10; i++) {
			sharpTV.brightnessUp();
			sharpTV.contrastDown();
			sharpTV.pictureUp();
			System.out.println(sharpTV);
		}
		
	}

}
