/**
 * 
 */
package com.elavon.tutorial.recursion;

/**
 * Exercise for recursive function using factorial.
 * @author Randy Roman
 *
 */
public class Factorial {

	private int nResult = 1;
	
	/**
	 * 
	 */
	public Factorial() {
	}
	
	public int f(int n) {
		if (nResult == 1 && n == 0) 
			return 0;
		else {
			if (n > 1) {
				nResult = n * f(n-1);
			}
			
		}
		
		return nResult;
	}
	
	public void reset( ) {
		nResult = 1;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Factorial factorial = new Factorial();
		for (int n=0; n<=10; n++) {
			factorial.reset();
			System.out.println("Factorial of " + n + " = " + factorial.f(n));
		}
	}

}
